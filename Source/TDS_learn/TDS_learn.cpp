// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_learn.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_learn, "TDS_learn" );

DEFINE_LOG_CATEGORY(LogTDS_learn)
 