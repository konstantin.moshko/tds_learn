// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS_learn : ModuleRules
{
	public TDS_learn(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
