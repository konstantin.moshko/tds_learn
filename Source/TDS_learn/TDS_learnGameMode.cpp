// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_learnGameMode.h"
#include "TDS_learnPlayerController.h"
#include "TDS_learnCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDS_learnGameMode::ATDS_learnGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS_learnPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}