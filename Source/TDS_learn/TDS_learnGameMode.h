// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDS_learnGameMode.generated.h"

UCLASS(minimalapi)
class ATDS_learnGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDS_learnGameMode();
};



